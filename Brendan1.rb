class Person
	def hello
		puts "Hello!"
	end
	
	def hello_introduce
		hello
		introduce
	end
	
	private
		def introduce
			puts "I am a person"
		end	
end
	

class Student < Person
	def Introduce
		puts "I am a student"
	end
	
end

stu = Student.new
stu.Introduce
stu.hello_introduce
per = Person.new
per.hello_introduce