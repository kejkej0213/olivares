class Book
	attr_accessor :title, :price
	def initialize(title, price)
		@title = title
		@price = price
	end
	def price_in_cents
		Integer(@price * 100 + 0.5)
	end
end

book = Book.new("Ruby Programming", 100.50)
puts book.price_in_cents